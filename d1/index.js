
let express = require('express');
let mongoose = require("mongoose");
let app = express();
const PORT = 5000;


let tasksRoutes = require('./routes/tasksRoutes');

//middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect("mongodb+srv://JoannaMC:admin@cluster0.wx5pn.mongodb.net/s31?retryWrites=true&w=majority",
	{
		useNewUrlParser:true,
		useUnifiedTopology: true
	}
).then(()=> console.log(`Connected to Database`))
.catch((error)=> console.log(error));

//schema
//routes

app.use("/", tasksRoutes);

app.listen(PORT,()=> console.log(`Server running on port ${PORT}`));
